# Phase transitions in autonomous intersection traffic



<img src="figs/intersection_schematic.png"  width="40%" height="40%">
<img src="figs/scheduling.png"  width="40%" height="40%">

<img src="figs/phase_diagram_fcfs.png" alt="phase diagram for fcfs scheduling by dimitra maoutsa" width="40%" height="40%">   



<img src="figs/platoon_promoting_phase_diagram.png" alt="phase diagram for platoon promoting scheduling by dimitra maoutsa" width="40%" height="40%">


<img src="figs/Cost_N1_vs__rate_resc.png" alt="finite size scaling for cost vs rate by dimitra maoutsa" width="40%" height="40%">



Giant component emerging in the network for increasing incoming rate (rescaled):

<img src="figs/Giant_N1_vs__rate_.png" alt="finite size scaling for size of giant component vs rate by dimitra maoutsa" width="40%" height="40%">


Considering vehicle platoons of size N:

<img src="figs/N10all_cost_rescSS.png"  width="40%" height="40%">
<img src="figs/N10all_cost_susc_rescSS.png"  width="40%" height="40%">



